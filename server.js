const express = require("express");
const bodyParser = require("body-parser");
let jwt = require("jsonwebtoken");
let config = require("./config");
let middleware = require("./middleware");
let os = require("os");
class HandlerGenerator {
    login (req,res){
        let username = req.body.username;
        let password = req.body.password;

        let mockedUsername = 'admin';
        let mockedPassword = 'password';

        if(username && password){
            if(username === mockedUsername && password === mockedPassword){
                let token = jwt.sign(
                    {username:username},
                    config.secret,
                    {expiresIn: '24h'}
                );
                res.json({
                    success: true,
                    message: "Authentication sucessfull!",
                    token: token,
                    hostname: os.hostname()
                });
            }else{
                res.send(403).json({
                    success: false,
                    message: "Incorrect username or password",
                    hostname: os.hostname()
                });
            }
        }else{
            res.send(400).json({
                success: false,
                message: "Auth failed please check the request",
                hostname: os.hostname()
            });
        }
    }
    index (req,res){
        res.json({
            success:true,
            message: "Index page",
            hostname: os.hostname()
        });
    }
}
function main(){
    let app= express();
    let handlers = new HandlerGenerator();
    const port = process.env.port || 8000;
    const host = process.env.host || "0.0.0.0";
    console.log(os.hostname());
    app.use(bodyParser.urlencoded({ //Middleware
        extended: true
    }));
    app.use(bodyParser.json());

    //Rutas
    app.post('/login', handlers.login);
    app.get("/", middleware.checkToken, handlers.index);
    app.listen(port, () => console.log(`Server running on: ${host}:${port}`))
}

main();