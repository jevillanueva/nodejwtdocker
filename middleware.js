let jwt= require("jsonwebtoken");
const config = require("./config.js");
let os = require("os");
let checkToken = (req,res,next)=>{
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    if(token !== undefined){
        if (token.startsWith('Bearer ')) {
            token = token.slice(7,token.length);
        }
    }
    if(token){
        jwt.verify(token,config.secret,(err,decoded)=> {
            if(err){
                return res.json({
                    success: false,
                    message: "Token is not valid",
                    hostname: os.hostname()
                });
            }else{
                req.decoded = decoded;
                next();
            }
        });
    }else{
        return res.json({
            success: false,
            message: "Token is not supplied",
            hostname: os.hostname()
        });
    }
};

module.exports = {
    checkToken: checkToken
}